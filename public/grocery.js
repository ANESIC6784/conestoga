import 'https://cdnjs.cloudflare.com/ajax/libs/framework7/5.7.10/js/framework7.bundle.js';
import "https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.0/firebase-app.min.js";
import "https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.0/firebase-database.min.js";
import "https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.1/firebase-auth.min.js";
import app from "./F7App.js";

const $$ = Dom7;

$$("#tab2").on("tab:show", () => {
    //put in firebase ref here
    const sUser = firebase.auth().currentUser.uid;
    firebase.database().ref("books/").on("value", (snapshot) =>{
        const oItems = snapshot.val();
        const aKeys = Object.keys(oItems);
        $$("#bookList").html("");
        for(let n = 0; n < aKeys.length; n++){
            let sRow ="";
            if(oItems[aKeys[n]].dateRead){
                console.log("Read: ", oItems[aKeys[n]]);
                sRow += `<div class="rowComp">`;
                console.log("added rowComp");
            } else { sRow += `<div class="card">` }
            sRow += `<div class="card-content card-content-padding">
                <h2>${oItems[aKeys[n]].title} |
               ${oItems[aKeys[n]].author} <h2>
               <h4>${oItems[aKeys[n]].published} | 
               ${oItems[aKeys[n]].genre}
               <button id="d${aKeys[n]}" class="delete"> Nevermind </button>
               <button id="u${aKeys[n]}"> I Read It </button> </h4>
               </div>
            </div>`;
            $$("#bookList").append(sRow);
        }
    });

});

$$(".my-sheet").on("submit", e => {
    //submitting a new note
    e.preventDefault();
    const oData = app.form.convertToData("#addItem");
    const sUser = firebase.auth().currentUser.uid;
    const sId = new Date().toISOString().replace(".", "_");
    firebase.database().ref("books/" + sId).set(oData);
    app.sheet.close(".my-sheet", true);
});



$$("#bookList").on("click", (evt)=>{
    evt.preventDefault();
    const sId = evt.target.id;
    if(sId[0] =='d'){
        console.log(sId);
        firebase.database().ref(`books/${sId.substr(1)}`).remove();
    }
    else if(sId[0] =='u'){
        const sDateRead = new Date().toISOString().replace(".", "_");
        firebase.database().ref(`books/${sId.substr(1)}/dateRead`).set(sDateRead);
    }
});